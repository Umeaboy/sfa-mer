TOOLDIR="$(dirname $0)/../.."

. "$TOOLDIR/utility-functions.inc"

. ~/.hadk.env

pushd ./device/sony/pioneer
    rm -f cm.dependencies
    git checkout lineage.dependencies
popd
sed -i -n '/kernel/{N;s/.*//;x;d;};x;p;${x;p;}' ./device/sony/pioneer/lineage.dependencies
sed -i "/},$/d" ./device/sony/pioneer/lineage.dependencies
sed -i "/^$/d"  ./device/sony/pioneer/lineage.dependencies


cp /$USER/pioneermanifest.xml /$USER/android/lineage/.repo/local_manifests/pioneermanifest.xml
sed -i "/_sony_pioneer/d" .repo/manifests/default.xml
